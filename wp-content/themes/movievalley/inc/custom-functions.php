<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package movievalley
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function movievalley_body_classes( $classes ) {
    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    // Adds a class of no-sidebar when there is no sidebar present.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'no-sidebar';
    }

    return $classes;
}
add_filter( 'body_class', 'movievalley_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function movievalley_pingback_header() {
    if ( is_singular() && pings_open() ) {
        echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
    }
}
add_action( 'wp_head', 'movievalley_pingback_header' );
function movievalley_get_header_image($id, $meta_id, $meta_key = 'header-image')
{
    $meta_data = get_post_meta($id, $meta_id, true);
    return isset($meta_data[$meta_key]) && !empty($meta_data[$meta_key]) ?
        wp_get_attachment_image($meta_data[$meta_key], 'owl-carosell', array('class'=>'img-responsive')) : '';
}
function movie_star_counter($rat){
    $rating= (float)$rat;
    $div=intval($rating)/2;
    if (is_float($div)) {
        $c=true;
        $div=intval($div);
    } else{
        $c=false;
    }
    for ($i=1;$i<=$div;$i++){
        ?>
        <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
    <?php }
    if ($c){
        ?>
        <li><a href="#"><i class="fa fa-star-half-o" aria-hidden="true"></i></a></li>
        <?php
        for ($i=1;$i<=(5-($div+1));$i++){?>

            <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
        <?php }
    } else{ for ($i=1;$i<=(5-$div);$i++){?>

        <li><a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
    <?php }}
}
function get_breadcrumb() {
    echo '<li><a href="'.home_url().'" rel="nofollow">Home</a></li>';
    if (is_category()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo '<li><a href="' . the_category(' &bull; ') . '" rel="nofollow">' . the_category(' &bull; ') . '</a></li>';
    }
    elseif (is_single()) {
            echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
            echo '<li><a href="'.the_title().'" rel="nofollow">'.the_title().'</a></li>';
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo '<li><a href="'.the_title().'" rel="nofollow">'.the_title().'</a></li>';
    } elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo '<li><a href="'.the_search_query().'" rel="nofollow">'.the_search_query().'</a></li>';
        echo '</em>"';
    }
}
function ia_comment_form_text ($defaults) {
    $defaults['label_submit'] = 'Add Comment';
    $commenter = wp_get_current_commenter();
    $defaults['title_reply'] = '<a href="#">Comments</a>';
    $defaults['id_form'] = '';
    $defaults['class_form'] = '';
    $defaults['comment_field'] = ' <textarea id="comment" name="comment" placeholder="Message" required=""></textarea>';
    return $defaults;
}
add_filter('comment_form_defaults','ia_comment_form_text');

// Remove Fields on Comment Form
function ia_comment_form_fields($fields){
    $fields['email'] = '<input type="text" id="email" name="email" placeholder="Email" required="">';
    return $fields;
}
add_filter('comment_form_default_fields','ia_comment_form_fields');
function wpse_comment_callback($comment, $depth, $args){
?>
    <?php
//    echo '<pre>';
//    var_dump($comment );
//    echo '</pre>';
    ?>
        <div class="media">
            <h5><?php print($comment->comment_author); ?></h5>
            <div class="media-left">
                <a href="#">
                    <img src="<?php echo get_avatar_url( $comment->user_id,96 ) ; ?>" title="One movies" alt=" " />
                </a>
            </div>
            <div class="media-body">
                <p><?php print($comment->comment_content); ?></p>
                <span>View all posts by :<a href="<?php echo  get_author_posts_url( $comment->user_id ) ; ?>"> <?php echo get_userdata($comment->user_id)->display_name; ?> </a></span>
            </div>
        </div>
<?php
}

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
?>