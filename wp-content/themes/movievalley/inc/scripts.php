<?php
/**
 * Enqueue scripts and styles.
 */
function movievalley() {
    wp_enqueue_style( 'movievalley-font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-old-style', get_stylesheet_directory_uri() . '/assets/css/old-style.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-contactstyle', get_stylesheet_directory_uri() . '/assets/css/contactstyle.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-faqstyle', get_stylesheet_directory_uri() . '/assets/css/faqstyle.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-medile', get_stylesheet_directory_uri() . '/assets/css/medile.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-jquery-slidey', get_stylesheet_directory_uri() . '/assets/css/jquery.slidey.min.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-popuo-box', get_stylesheet_directory_uri() . '/assets/css/popuo-box.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-font-awesome', get_stylesheet_directory_uri() . '/assets/css/font-awesome.min.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-flexslider', get_stylesheet_directory_uri() . '/assets/css/flexslider.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-owl-carousel', get_stylesheet_directory_uri() . '/assets/css/owl.carousel.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-single', get_stylesheet_directory_uri() . '/assets/css/single.css', array(), '1.0.0' );
    wp_enqueue_style( 'movievalley-style', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), '1.0.0' );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri(). '/assets/js/bootstrap.min.js', array('jquery'), ' ');
    wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri().'/assets/js/owl.carousel.js', array('jquery'), ' ', true );
    wp_enqueue_script( 'jquery-slidey-js', get_template_directory_uri().'/assets/js/jquery.slidey.js', array('jquery'), ' ', true );
    wp_enqueue_script( 'jquery.dotdotdot.min.js', get_template_directory_uri().'/assets/js/jquery.dotdotdot.min.js', array('jquery'), ' ', true );
    wp_enqueue_script( 'jquery-flexslider-js', get_template_directory_uri().'/assets/js/jquery.flexslider.js', array('jquery'), ' ', true );
    wp_enqueue_script( 'jquery-magnific-popup-js', get_template_directory_uri().'/assets/js/jquery.magnific-popup.js', array('jquery'), ' ', true );
    wp_enqueue_script( 'simplePlayer', get_template_directory_uri().'/assets/js/simplePlayer.js', array('jquery'), ' ', true );
    wp_enqueue_script( 'movievalley-custom-js', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), ' ', true );
}
add_action( 'wp_enqueue_scripts', 'movievalley' );


/**
 * Filter the HTML script tag of `leadgenwp-fa` script to add `defer` attribute.
 *
 */
function movievalley_defer_scripts( $tag, $handle, $src ) {
    // The handles of the enqueued scripts we want to defer
    $defer_scripts = array(
        'movievalley-fa'
    );
    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer></script>';
    }
    return $tag;
}
add_filter( 'script_loader_tag', 'movievalley_defer_scripts', 10, 3 );
?>