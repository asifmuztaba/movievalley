<?php
/**
 * Template Name: Home page
 */
get_header();
get_template_part('template-parts/home-main-slider');
get_template_part('template-parts/home-bottom-banner');
get_template_part('template-parts/home-movies');
get_footer();
?>