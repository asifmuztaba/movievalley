<!-- banner-bottom -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">

<div class="sd-left">
<div class="banner-bottom">
    <div class="container">
        <div class="w3_agile_banner_bottom_grid">
            <div id="owl-demo" class="owl-carousel owl-theme">
                <?php
                $args = array(
                    'numberposts' => 10,
                    'offset' => 0,
                    'category' => 0,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'include' => '',
                    'exclude' => '',
                    'meta_key' => '',
                    'meta_value' =>'',
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'suppress_filters' => true
                );

                $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                foreach($recent_posts as $p):
                    $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
                    $meta = $postmeta[0]['header-image'];
                    //var_dump(movievalley_get_header_image($p["ID"],'movievalley-post-meta'));
                    ?>
                    <div class="item">
                        <div class="w3l-movie-gride-agile w3l-movie-gride-agile1">
                            <a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>" class="hvr-shutter-out-horizontal"><?php echo movievalley_get_header_image($p["ID"],'movievalley-post-meta')?>
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>"><?php echo $p['post_title']?></a></h6>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><?php echo $postmeta[0]['mvi-published'];?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
<?php movie_star_counter($postmeta[0]['mvi-rating']);?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="ribben">
                                <p>NEW</p>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>
<!-- //banner-bottom -->