<div id="slidey" style="display:none;">
    <ul>
        <?php
        $args = array(
            'numberposts' => 10,
            'offset' => 0,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
        foreach($recent_posts as $p):
            $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
            $meta = get_the_post_thumbnail( $p["ID"],  $size = 'post-thumbnail', $attr = '' );
            //var_dump($postmeta[0]);
            ?>
            <li> <?php echo $meta;?>
                <p class='title'><?php echo $p['post_title']?></p>
                <p class='description'><?php echo $postmeta[0]['mvi-desc']?></p></li>
        <?php
        endforeach;
        ?>
        <!--        <li><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/2.jpg" alt=" "><p class='title'>Maximum Ride</p><p class='description'>Six children, genetically cross-bred with avian DNA, take flight around the country to discover their origins. Along the way, their mysterious past is ...</p></li>-->
        <!--        <li><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/3.jpg" alt=" "><p class='title'>Independence</p><p class='description'>The fate of humanity hangs in the balance as the U.S. President and citizens decide if these aliens are to be trusted ...or feared.</p></li>-->
        <!--        <li><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/4.jpg" alt=" "><p class='title'>Central Intelligence</p><p class='description'>Bullied as a teen for being overweight, Bob Stone (Dwayne Johnson) shows up to his high school reunion looking fit and muscular. Claiming to be on a top-secret ...</p></li>-->
        <!--        <li><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/6.jpg" alt=" "><p class='title'>Ice Age</p><p class='description'>In the film's epilogue, Scrat keeps struggling to control the alien ship until it crashes on Mars, destroying all life on the planet.</p></li>-->
        <!--        <li><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/7.jpg" alt=" "><p class='title'>X - Man</p><p class='description'>In 1977, paranormal investigators Ed (Patrick Wilson) and Lorraine Warren come out of a self-imposed sabbatical to travel to Enfield, a borough in north ...</p></li>-->
    </ul>
</div>