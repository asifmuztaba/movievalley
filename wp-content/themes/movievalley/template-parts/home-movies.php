<div class="general">
    <h4 class="latest-text w3_latest_text">Featured Movies</h4>
    <div class="container">
        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Featured</a></li>
                <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">Top viewed</a></li>
                <li role="presentation"><a href="#rating" id="rating-tab" role="tab" data-toggle="tab" aria-controls="rating" aria-expanded="true">Top Rating</a></li>
                <li role="presentation"><a href="#imdb" role="tab" id="imdb-tab" data-toggle="tab" aria-controls="imdb" aria-expanded="false">Recently Added</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                    <div class="w3_agile_featured_movies">
                        <?php
                        $args = array(
                            'numberposts' => 10,
                            'offset' => 0,
                            'category' => 0,
                            'orderby' => 'post_date',
                            'order' => 'DESC',
                            'include' => '',
                            'exclude' => '',
                            'meta_key' => '',
                            'meta_value' =>'',
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'suppress_filters' => true
                        );

                        $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                        foreach($recent_posts as $p):
                        $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
                        $meta = $postmeta[0]['header-image'];
                        //var_dump(movievalley_get_header_image($p["ID"],'movievalley-post-meta'));
                        ?>

                        <div class="col-md-2 w3l-movie-gride-agile">
                            <a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>" class="hvr-shutter-out-horizontal"><?php echo movievalley_get_header_image($p["ID"],'movievalley-post-meta')?>
                                <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                            </a>
                            <div class="mid-1 agileits_w3layouts_mid_1_home">
                                <div class="w3l-movie-text">
                                    <h6><a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>"><?php echo $p['post_title']?></a></h6>
                                </div>
                                <div class="mid-2 agile_mid_2_home">
                                    <p><?php echo $postmeta[0]['mvi-published'];?></p>
                                    <div class="block-stars">
                                        <ul class="w3l-ratings">
                                            <?php movie_star_counter($postmeta[0]['mvi-rating']);?>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="ribben">
                                <p>NEW</p>
                            </div>
                        </div>
                        <?php endforeach;?>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                    <?php
                    $args = array(
                        'numberposts' => 10,
                        'offset' => 0,
                        'category' => 0,
                        'orderby' => 'post_date',
                        'order' => 'ASC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' =>'',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                    );

                    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                    foreach($recent_posts as $p):
                    $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
                    $meta = $postmeta[0]['header-image'];
                    //var_dump(movievalley_get_header_image($p["ID"],'movievalley-post-meta'));
                    ?>
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>" class="hvr-shutter-out-horizontal"><?php echo movievalley_get_header_image($p["ID"],'movievalley-post-meta')?>
                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>
                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>"><?php echo $p['post_title']?></a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p><?php echo $postmeta[0]['mvi-published'];?></p>
                                <div class="block-stars">
                                    <ul class="w3l-ratings">
                                        <?php movie_star_counter($postmeta[0]['mvi-rating']);?>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="clearfix"> </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="rating" aria-labelledby="rating-tab">
                    <?php
                    $args = array(
                        'numberposts' => 10,
                        'offset' => 0,
                        'category' => 0,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' =>'',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                    );

                    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                    foreach($recent_posts as $p):
                    $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
                    $meta = $postmeta[0]['header-image'];
                    //var_dump(movievalley_get_header_image($p["ID"],'movievalley-post-meta'));
                    ?>
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>" class="hvr-shutter-out-horizontal"><?php echo movievalley_get_header_image($p["ID"],'movievalley-post-meta')?>
                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>
                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>"><?php echo $p['post_title']?></a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p><?php echo $postmeta[0]['mvi-published'];?></p>
                                <div class="block-stars">
                                    <ul class="w3l-ratings">
                                        <?php movie_star_counter($postmeta[0]['mvi-rating']);?>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="clearfix"> </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="imdb" aria-labelledby="imdb-tab">
                    <?php
                    $args = array(
                        'numberposts' => 10,
                        'offset' => 0,
                        'category' => 0,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' =>'',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                    );

                    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                    foreach($recent_posts as $p):
                    $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
                    $meta = $postmeta[0]['header-image'];
                    //var_dump(movievalley_get_header_image($p["ID"],'movievalley-post-meta'));
                    ?>
                    <div class="col-md-2 w3l-movie-gride-agile">
                        <a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>" class="hvr-shutter-out-horizontal"><?php echo movievalley_get_header_image($p["ID"],'movievalley-post-meta')?>
                            <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                        </a>
                        <div class="mid-1 agileits_w3layouts_mid_1_home">
                            <div class="w3l-movie-text">
                                <h6><a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>"><?php echo $p['post_title']?></a></h6>
                            </div>
                            <div class="mid-2 agile_mid_2_home">
                                <p><?php echo $postmeta[0]['mvi-published'];?></p>
                                <div class="block-stars">
                                    <ul class="w3l-ratings">
                                        <?php movie_star_counter($postmeta[0]['mvi-rating']);?>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="ribben">
                            <p>NEW</p>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- //general -->
<!-- Latest-tv-series -->