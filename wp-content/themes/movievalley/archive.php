<?php
//Template Name: Generse
get_header();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">

            <div class="sd-left">
<div class="general-agileits-w3l">
    <div class="w3l-medile-movies-grids">

        <!-- /movie-browse-agile -->

        <div class="movie-browse-agile">
            <!--/browse-agile-w3ls -->
            <div class="browse-agile-w3ls general-w3ls">
                <div class="tittle-head">
                    <h4 class="latest-text"><?php echo get_the_category()[0]->name;?> </h4>

                    <div class="container">
                        <div class="agileits-single-top">
                            <ol class="breadcrumb">
                                <li><a href="<?php site_url();?>">Home</a></li>
                                <li class="active"><?php echo get_the_category()[0]->name;?></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="browse-inner">
        <?php
        $cat_id=get_the_category()[0]->cat_ID;
        $args = array(
            'numberposts' => 10,
            'offset' => 0,
            'category' => $cat_id,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
        foreach($recent_posts as $p):
            $postmeta = get_post_meta( $p["ID"], $key = 'movievalley-post-meta', $single = false );
            $meta = $postmeta[0]['header-image'];
            ?>
                            <div class="col-md-2 w3l-movie-gride-agile">
                                <a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>" class="hvr-shutter-out-horizontal"><?php echo movievalley_get_header_image($p["ID"],'movievalley-post-meta')?>
                                    <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                                </a>
                                <div class="mid-1">
                                    <div class="w3l-movie-text">
                                        <h6><a href="<?php echo get_permalink( $p['ID'],  $leavename = false )?>"><?php echo $p['post_title']?></a></h6>
                                    </div>
                                    <div class="mid-2">

                                        <p><?php echo $postmeta[0]['mvi-published'];?></p>
                                        <div class="block-stars">
                                            <ul class="w3l-ratings">
                                                <?php movie_star_counter($postmeta[0]['mvi-rating']);?>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                                <div class="ribben two">
                                    <p>NEW</p>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="clearfix"> </div>
                    </div>

                </div>
                <!--//browse-agile-w3ls -->
                <div class="blog-pagenat-wthree">
                    <ul>
                        <li><a class="frist" href="<?php next_posts_link( 'Older posts' ); ?>">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a class="last" href="<?php previous_posts_link( 'Newer posts' ); ?>">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- //movie-browse-agile -->
<!--body wrapper start-->
<!--body wrapper start-->
<?php
get_footer();
?>
