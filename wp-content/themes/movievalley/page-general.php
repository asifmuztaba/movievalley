<?php
//Template Name: General Pages
get_header();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-9">
<div class="sd-left">

                <div class="extra-pages">
                    <?php
                    while (have_posts()):the_post();
                        the_content();
                    endwhile;
                    ?>
                </div>
<?php
get_footer();
?>
