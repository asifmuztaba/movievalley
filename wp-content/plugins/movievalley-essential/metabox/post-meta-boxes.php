<?php

add_filter('cs_metabox_options', function ( $options ) {
    $options[] = array(
        'id' => 'page-meta',
        'title' => 'Page Meta Setup',
        'post_type' => 'page', // or post or CPT or array( 'page', 'post' )
        'context' => 'normal',
        'priority' => 'default',
        'sections' => array(
            array(
                'name' => 'page-common-settings',
                'title' => 'Common Settings',
                'icon' => 'fa fa-wifi',
                'fields' => array(
                    array(
                        'id' => 'header-image',
                        'type' => 'image',
                        'title' => 'Header Image',
                    ),
                    array(
                        'id' => 'header_image_text',
                        'type' => 'textarea',
                        'title' => 'Header Image Text',
                    ),
                    array(
                        'id' => 'header_image_type',
                        'type' => 'text',
                        'title' => 'Header Image Type',
                    )
                ),
            ),
            array(
                'name' => 'page-about',
                'title' => 'About page setting',
                'icon' => 'fa fa-female',
                'fields' => array(
                    array(
                        'id' => 'about-page-section',
                        'type' => 'group',
                        'title' => 'About page section',
                        'button_title' => 'Add New',
                        'accordion_title' => 'section-title',
                        'fields' => array(
                            array(
                                'id' => 'section-title',
                                'type' => 'text',
                                'title' => 'Title',
                            ),
                            array(
                                'id' => 'section-image',
                                'type' => 'image',
                                'title' => 'Section image for left side',
                            ),
                            array(
                                'id' => 'section-description',
                                'type' => 'textarea',
                                'title' => 'Section description',
                            ),
                        ),

                    ),
                    array(
                        'id' => 'linked-testimonial-about',
                        'type' => 'select',
                        'title' => 'Select a group of testimonial',
                        'options' => 'categories',
                        'query_args' => array(
                            'type' => 'ryc-testimonial',
                            'taxonomy' => 'ryc-testimonial-tag',
                            'orderby' => 'post_date',
                            'order' => 'DESC',
                            'hide_empty' => false
                        ),
                    ),
                ),
            ),
            array(
                'name' => 'courses-main-page',
                'title' => 'Courses main page settings',
                'icon' => 'fa fa-video-camera',
                'fields' => array(
                    array(
                        'id' => 'feature-course',
                        'type' => 'fieldset',
                        'title' => 'Set feature course',
                        'fields' => array(
                            array(
                                'id' => 'feature-post',
                                'type' => 'select',
                                'options' => 'posts',
                                'title' => 'Select course',
                                'query_args' => array(
                                    'post_type' => 'lauren-courses',
                                    'posts_per_page' => -1,
                                    'orderby' => 'post_date',
                                    'order' => 'DESC',
                                ),
                                'default_option' => 'Select a course',
                            ),
                            array(
                                'id' => 'title',
                                'type' => 'text',
                                'title' => 'Title',
                            ),
                            array(
                                'id' => 'description',
                                'type' => 'textarea',
                                'title' => 'Small description',
                            ),
                            array(
                                'id' => 'video-banner',
                                'type' => 'image',
                                'title' => 'Video background image',
                            ),
                            array(
                                'id' => 'video-url',
                                'type' => 'text',
                                'title' => 'Video url',
                            ),
                        )
                    ),
                ),
            ),
            array(
                'name' => 'tt-page',
                'title' => 'TT page settings',
                'icon' => 'fa fa-wifi',
                'fields' => array(
                    array(
                        'id' => 'tt-name',
                        'type' => 'text',
                        'title' => 'Name',
                    ),
                    array(
                        'id' => 'tt-faq-link',
                        'type' => 'text',
                        'title' => 'Faq link',
                    ),
                    array(
                        'id' => 'tt-big-banner',
                        'type' => 'image',
                        'title' => 'Big banner',
                    ),
                    array(
                        'id' => 'tt-signup-link',
                        'type' => 'text',
                        'title' => 'Sign up link',
                    ),
                    array(
                        'id' => 'price-details',
                        'type' => 'text',
                        'title' => 'price details',
                    ),
                    array(
                        'id' => 'location',
                        'type' => 'text',
                        'title' => 'Location',
                    ),
                    array(
                        'id' => 'short-date',
                        'type' => 'text',
                        'title' => 'Short date',
                    ),
                    array(
                        'id' => 'long-date',
                        'type' => 'text',
                        'title' => 'Long date',
                    ),
                ),
            ),
            array(
                'name' => 'faq-meta',
                'title' => 'Faq setup',
                'icon' => 'fa fa-wifi',
                'fields' => array(
                    array(
                        'id' => 'top-logo-faq',
                        'type' => 'image',
                        'title' => 'Top image logo',
                    ),
                    array(
                        'id' => 'tt-faq',
                        'type' => 'group',
                        'title' => 'TT faq',
                        'button_title' => 'Add New',
                        'accordion_title' => 'faq-title',
                        'fields' => array(
                            array(
                                'id' => 'faq-title',
                                'type' => 'textarea',
                                'title' => 'FAQ title'
                            ),
                            array(
                                'id' => 'faq-content',
                                'type' => 'textarea',
                                'title' => 'Faq content'
                            ),
                        ),
                    ),
                    array(
                        'id' => 'faq-type',
                        'type' => 'text',
                        'title' => 'Faq Page Type',
                    ),
                ),
            ),
            array(
                'name' => 'teacher-training-combo-box-setup',
                'title' => 'Teacher training setup',
                'icon' => 'fa fa-wifi',
                'fields' => array(
                    array(
                        'id' => 'additional-product',
                        'type' => 'text',
                        'title' => 'Additional product id',
                    )
                ),
            ),

        ),
    );
    $options[] = array(
        'id' => 'movievalley-post-meta',
        'title' => 'Post Meta option',
        'post_type' => 'post', // or post or CPT or array( 'page', 'post' )
        'context' => 'normal',
        'priority' => 'default',
        'sections' => array(
            array(
                'name' => 'common-settings',
                'title' => 'Common Settings',
                'icon' => 'fa fa-wifi',
                'fields' => array(
                    array(
                        'id' => 'header-image',
                        'type' => 'image',
                        'title' => 'Header Image',
                    ),
                    array(
                        'id' => 'mvi-desc',
                        'type' => 'textarea',
                        'title' => 'Movie Short Description',
                    ),
                ),
            ),
            array(
                'name' => 'movie-settings',
                'title' => 'Movie setup',
                'icon' => 'fa fa-file-video-o',
                'fields' => array(
                    array(
                        'id' => 'movie-country',
                        'type' => 'select',
                        'title' => 'Select Country for this Movie',
                        'options' => 'posts',
                        'query_args' => array(
                            'post_type' => 'movievalley-country',
                            'posts_per_page' => -1,
                            'orderby' => 'post_date',
                            'order' => 'DESC',
                            'hide_empty' => true
                        ),
                    ),
                    array(
                        'id' => 'mvi-rating',
                        'type' => 'text',
                        'title' => 'Movie Rating',
                    ),
                    array(
                        'id' => 'mvi-published',
                        'type' => 'text',
                        'title' => 'Movie Published Year',
                    ),
                    array(
                        'id' => 'mvi-trailer',
                        'type' => 'textarea',
                        'title' => 'Movie Trailer URL',
                    ),
                ),
            ),

        ),
    );
    $options[] = array(
        'id' => 'ryc-home-slider',
        'title' => 'Home slider link',
        'post_type' => 'ryc-home-slider', // or post or CPT or array( 'page', 'post' )
        'context' => 'normal',
        'priority' => 'default',
        'sections' => array(
            array(
                'name' => 'settings-videos',
                'icon' => 'fa fa-file-video-o',
                'fields' => array(
                    array(
                        'id' => 'slider_text_up',
                        'type' => 'textarea',
                        'title' => 'Slider text up',
                    ),

                    array(
                        'id' => 'slider_text_bottom',
                        'type' => 'textarea',
                        'title' => 'Slider text bottom',
                    ),
                    array(
                        'id' => 'link',
                        'type' => 'text',
                        'title' => 'Slider Link',
                    ),
                    array(
                        'id' => 'sp-class',
                        'type' => 'text',
                        'title' => 'Special css class',
                        'sanitize' => false
                    ),
                    array(
                        'id' => 'slider_type',
                        'type' => 'text',
                        'title' => 'Slider Type',
                    ),
                    array(
                        'id'=>'slider_location',
                        'type'=>'text',
                        'title'=>'Slider Page Location',
                    )
                ),
            ),
        ),
    );
    $options[] = array(
        'id' => 'movievalley-country-meta',
        'title' => 'Country Options',
        'post_type' => 'movievalley-country', // or post or CPT or array( 'page', 'post' )
        'context' => 'normal',
        'priority' => 'default',
        'sections' => array(
            array(
                'name' => 'country-option-meta',
                'title' => 'Country Settings',
                'icon' => 'fa fa-wifi',
                'fields' => array(
                    array(
                        'id' => 'quote',
                        'type' => 'wysiwyg',
                        'title' => 'Quote',
                    ),
                    array(
                        'id' => 'location',
                        'type' => 'text',
                        'title' => 'Location',
                    ),

                ),
            ),

        ),
    );
    return $options;
});