<?php
add_filter('cs_framework_settings', function ($settings) {
    $settings = array(
        'menu_title' => 'MovieValley',
        'menu_type' => 'menu', // menu, submenu, options, theme, etc.
        'menu_slug' => 'cs-framework',
        'ajax_save' => false,
        'show_reset_all' => false,
        'framework_title' => 'This is Awesome options for movievalley ',
    );
    return $settings;
});
add_filter('cs_framework_options', function ($options) {
    $options = array();
    $options[] = array(
        'name' => 'backup_section',
        'title' => 'Backup',
        'icon' => 'fa fa-shield',
        'fields' => array(

            array(
                'type' => 'notice',
                'class' => 'warning',
                'content' => 'You can save your current options. Download a Backup and Import.',
            ),

            array(
                'type' => 'backup',
            ),

        )
    );
    return $options;
});