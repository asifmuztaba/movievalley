<?php
/**
 * Created by PhpStorm.
 * User: jitu
 * Date: 6/23/2016
 * Time: 12:14 AM
 */
// Register Custom Post Type
function movievalley_country()
{

    $labels = array(
        'name' => _x('Country', 'Post Type General Name', 'movievalley'),
        'singular_name' => _x('Country', 'Post Type Singular Name', 'movievalley'),
        'menu_name' => __('Country', 'movievalley'),
        'name_admin_bar' => __('Country', 'movievalley'),
        'archives' => __('Item Archives', 'movievalley'),
        'parent_item_colon' => __('Parent Item:', 'movievalley'),
        'all_items' => __('All Items', 'movievalley'),
        'add_new_item' => __('Add New Item', 'movievalley'),
        'add_new' => __('Add New', 'movievalley'),
        'new_item' => __('New Item', 'movievalley'),
        'edit_item' => __('Edit Item', 'movievalley'),
        'update_item' => __('Update Item', 'movievalley'),
        'view_item' => __('View Item', 'movievalley'),
        'search_items' => __('Search Item', 'movievalley'),
        'not_found' => __('Not found', 'movievalley'),
        'not_found_in_trash' => __('Not found in Trash', 'movievalley'),
        'featured_image' => __('Featured Image', 'movievalley'),
        'set_featured_image' => __('Set featured image', 'movievalley'),
        'remove_featured_image' => __('Remove featured image', 'movievalley'),
        'use_featured_image' => __('Use as featured image', 'movievalley'),
        'insert_into_item' => __('Insert into item', 'movievalley'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'movievalley'),
        'items_list' => __('Items list', 'movievalley'),
        'items_list_navigation' => __('Items list navigation', 'movievalley'),
        'filter_items_list' => __('Filter items list', 'movievalley'),
    );
    $args = array(
        'label' => __('Country', 'movievalley'),
        'description' => __('Post Type Description', 'movievalley'),
        'labels' => $labels,
        'supports' => array('title', 'thumbnail'),
        'hierarchical' => false,
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => false,
        'rewrite' => false,
        'capability_type' => 'page',
    );
    register_post_type('movievalley-country', $args);

}

add_action('init', 'movievalley_country', 0);