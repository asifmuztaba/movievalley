<?php

/**
 * @since             1.0.0
 *
 * @wordpress-plugin
 * Plugin Name:       movievalley essentials
 * Text Domain:       movievalley
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//enque admin script
add_action( 'admin_enqueue_scripts',function($hook){
    wp_enqueue_style( 'custom_wp_admin_css',plugins_url( '/admin-scripts/css/admin-style.css' , __FILE__ ) , false, '1.0.0' );
    wp_enqueue_script( 'admin-js' , plugins_url( '/admin-scripts/js/admin.js' , __FILE__ ) , array('jquery') , '1.0.0' , true );
});
require_once plugin_dir_path( __FILE__ ) . '/taxonomy.php';
require_once plugin_dir_path( __FILE__ ). 'custompost/country.php';
require_once plugin_dir_path( __FILE__ ) . '/metabox/post-meta-boxes.php'; //adding metaboxes
require_once plugin_dir_path( __FILE__ ) . '/metabox/taxonomy_metabox.php'; //taxonomies
require_once plugin_dir_path( __FILE__ ) . '/theme-option.php'; //theme options
require_once plugin_dir_path( __FILE__ ) . '/admin-shortcode.php'; // short code